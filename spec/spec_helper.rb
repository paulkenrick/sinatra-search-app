require_relative '../sinatra_search_app'
require 'rspec'
require 'rack/test'
require 'webmock/rspec'
require 'capybara'
require 'capybara/rspec'

WebMock.disable_net_connect!(allow_localhost: true)
Capybara.app = SinatraSearchApp

RSpec.configure do |config|
  include Rack::Test::Methods
  config.include Capybara::DSL
  config.include Capybara::RSpecMatchers
  config.formatter = :documentation

  def app
    SinatraSearchApp
  end
end
