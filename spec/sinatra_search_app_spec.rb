require_relative './spec_helper'

describe 'SinatraSearchApp' do
  before(:each) do
    WebMock::API.stub_request(:get, 'https://gist.githubusercontent.com/carlos-alberto/c72b9ba94e35447d9ce33b18038b25ad/raw/862d147a8f99c96d8e1dda2531a902947b576719/exercise-data.json').
      to_return(body: '[{"name": "First Person", "email": "first.person@example.com"}, {"name": "Second Person", "email": "second.person@example.com"}]')
    end

  describe 'when visiting the root page' do
    it "displays all results" do
      visit '/'
      expect(page).to have_content('First Person')
      expect(page).to have_content('Second Person')
    end
  end

  describe 'when using search function' do
    before(:each) do
      visit '/'
      fill_in 'search_term', with: "second"
      click_button 'submit_button'
    end

    it "displays only the records matching the search term" do
      expect(page).to_not have_content('First Person')
      expect(page).to have_content('Second Person')
    end

    describe 'when clearing the search results' do
      it "displays only the records matching the search term" do
        click_on "clear_search_link"
        expect(page).to have_content('First Person')
        expect(page).to have_content('Second Person')
      end
    end
  end
end
