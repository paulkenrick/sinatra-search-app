# Sinatra Search App

 This is a simple web app that allows a user to search contacts held at https://gist.github.com/carlos-alberto/c72b9ba94e35447d9ce33b18038b25ad. It presents the user with a search box and a list of all the people who match the current search. When there is no search term it displays all the contacts. The app is functional only and has not been styled

This app has been built in Ruby using the Sinatra web framework.

## Getting Started

1. Clone the project
2. Use the Bundler gem to install the required project gems (run `bundler install`)
3. Use Rackup to run a local server (run `bundle exec rackup -p 3000`)
4. Visit `http://localhost:3000`

## Running the tests

Feature tests have been included using RSpec and Capybara.  To run these tests simply run `rspec`.

The tests make use of WebMock to stub out the external requests to the Github endpoint.

## Other Notes from the Author

*Initial Attempt*

Because I'm most familiar with it, I initially built this app using Ruby on Rails.  Although it initially seemed like overkill, a quick bit of research confirmed it was straight forward to remove the database and the other parts of Rails that were not required for a simple web app like this one.  

Although it was possible, the result felt very untidy. I therefore built it again using Sinatra, a framework I have not used for quite some time.  The result is much simpler and cleaner.  Feel free to look at the Rails attempt [here](https://gitlab.com/paulkenrick/search-app).

*Time Taken*

I estimate the initial Rails attempt took 2.5 hours of work (a lot of this time though was attempting to repurpose Rails to suit this simple app).  The final product in Sinatra took about 1 hour.
