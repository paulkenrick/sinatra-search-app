require 'sinatra/base'
require 'net/http'

class SinatraSearchApp < Sinatra::Base

  get '/' do
    url = 'https://gist.githubusercontent.com/carlos-alberto/c72b9ba94e35447d9ce33b18038b25ad/raw/862d147a8f99c96d8e1dda2531a902947b576719/exercise-data.json'
    uri = URI(url)
    response = Net::HTTP.get(uri)
    @contacts = JSON.parse(response)

    if !params[:search_term].nil? && params[:search_term] != ''
      @search_term = params[:search_term].downcase
      @contacts = @contacts.select do |contact|
        contact.values.join(' ').downcase.include?(@search_term)
      end
    end

    erb :contacts
  end
end
